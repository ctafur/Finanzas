Bitcoin
~~~~~~~

Bitcoin fue la primera criptomoneda que surgió, pues, en el paper_ que publicó
su creador (apodado Satoshi Nakamoto) y donde la presentó, solucionó el
principal problema que existía en la investigación para la creación de una
criptomoneda, hasta entonces irresoluble, un problema que se planteó hace mucho
en matemáticas y se conoce como el `problema de los generales bizantinos`_.

El mejor de los vídeos explicativos sobre el funcionamiento de la tecnología de
Bitcoin que he encontrado es `el de 3Blue1Brown`_, un creador de YouTube de vídeos
sobre matemáticas.

.. _el de 3Blue1Brown:  https://www.youtube.com/watch?v=bBC-nXj3Ng4&t=9s

Lo mejor sería que viese el vídeo anterior antes de continuar. Bitcoin consta
en realidad de varias partes. Al igual que la mayoría de plataformas de
criptoactivos (que surgieron posteriormente al Bitcoin), usa una tecnología
llamada blockchain, que es como una base de datos en una red peer-to-peer
[#bittorrent]_ (P2P). En esa base de datos se registran las transacciones y por
tanto es la que permite llevar la contabilidad de todos los usuarios. Existe
otra tecnología que hace funcionar a esa red de forma muy segura: el proof of
work.

.. [#peer-to-peer]
   Si lleva bastante tiempo en el mundo de los computadores, quizás recuerde
   que BitTorrent es una red de este tipo.

Sobre 2016 y 2017, hubo una explosión cámbrica de plataformas de criptoactivos,
muchas de ellas fraudulentas. También comenzaron a interesarse algunos
gobiernos en crear su propia criptomoneda o plataforma de criptoactivos. El
argumento que trataron de que calara en la sociedad es que lo único interesante
de Bitcoin era el blockchain. Su interés era tratar de desbancar a Bitcoin.
También han surgido plataformas de criptoactivos interesantes, como Ethereum,
por ejemplo. El caso es que ninguna ha llegado aún a cumplir con sus
espectativas. La única plataforma de criptoactivos que parece funcionar a
mediados de 2018 es Bitcoin, con sus inconvenientes, como el trabajo
computacional que se requiere para realizar las transacciones, el tiempo que se
requiere (alrededor de unos 10 minutos por transacción) y el gasto en energía.
Esos tres son los principales inconvenientes de Bitcoin. Estos tres
inconvenientes son consecuencia de una misma causa: el proof of work. Se están
tratando de idear soluciones para soslayar estos problemas, como la red
lightning (*lightning network*), que consiste en que algunas transacciones se
realicen fuera de la blockchain.




Forma de gobierno de Bitcoin :: mejor que Ethereum y otros, pues es más
distribuido. No existe una aristocracia que tome decisiones.









es el Blockchain, |ed|, la tecnología subyacente de Bitcoin. Se puede decir que
Bitcoin en realidad no es más que una aplicación de Blockchain, aunque, para
muchos es la *killer app*. En concreto, el Blockchain, o cualquier blockchain,
no es más que una base de datos [#ledger]_ que se encuentra replicada en muchos
nodos (computadores) y que, mediante la criptografía, se registra todo tipo de
cosas y no se puede alterar a menos que cuentes con más del 50% de poder
computacional de todos los computadores que forman esa red. Según dicen, su
creador hizo un uso muy inteligente de la teoría de juegos para que se pudiera
crear la criptomoneda.

.. _paper: bitcoin-paper_

.. [#ledger]
   Muchas veces dicen que es un *ledger*, que creo que es un libro de cuentas.
   En realidad, blockchain hace alusión a una base de datos.


Hay quien opina que Bitcoin es una red que malgasta muchos recursos. Es cierto
que, para realizar el proof of work y, por tanto, para que funcione Bitcoin, se
necesita bastante trabajo computacional. En la actualidad, el proof of work lo
llevan a cabo los *mineros*, que es como llaman a quienes realizan el proof of
work, dedicando equipos especializados para esa tarea. Actualmente, el tipo de
equipos que se usan para el minado son ASICs (*application-specific integrated
circuits*), aunque también hay quien lo hace con computadores con tarjetas
gráficas potentes (esta última era la opción que se usaba más al comienzo de
Bitcoin). Lo que está claro es que en la actualidad no es nava viable tratar de
minar por uno mismo con un computador personal. Para que sea rentable, se
necesita mucho mayor poder de cómputo.

Además del gasto en trabajo computacional que requiere Bitcoin para operar,
existe el problema del tiempo que se emplea en realizar una transacción. Ambas
cosas tienen relación, pues se deben al proof of work.






Ya hemos comentado los `Problemas de los blockchains`_. En Bitcoin, se están
probando distintas formas de solucionarlo, algunas se han implementado mediante
forks de Bitcoin.



|Ppej|, tenemos Bitcoin Cash. Vea también SegWit_. |Ttb| hay
quien explica que `no es tanto el malgasto de recursos`_ si se compara con el
sector financiero al que el Bitcoin sustituiría (dejaría obsoleto), sino que
seguramente saldríamos ganando. Además, en 2018 se está actualizando el
Blockchain (de Bitcoin) para que haga uso de la tecnología lightning network.
Además, los equipos que se encargan de hacer la operación de proof of work (lo
que también se conoce como "el minado"), cada vez son más eficientes; si se
sigue cumpliendo la ley de Moore, el problema en realidad no sería tan grave.

.. _SegWit: https://en.wikipedia.org/wiki/SegWit
.. _no es tanto el malgasto de recursos:
   https://hackernoon.com/
   the-bitcoin-vs-visa-electricity-consumption-fallacy-8cf194987a50

Otro de los problemas principales que se ha visto que tiene Bitcoin no es tanto
técnico como económico: la gente lo usa no como moneda de cambio sino como
reserva de valor (*store of value*), como se hace con el oro. Es evidente: si
esperas que suba mucho el valor del Bitcoin en relación a otra moneda que uses
normalmente, como puede ser el dólar americano (USD), es normal que tus pagos
los hagas en USD y no toques los Bitcoins que tienes. Esto, que sucede con
otras criptomonedas, en el caso del Bitcoin la hace más propicia a terminar
siendo reserva de valor únicamente, porque el diseño de Bitcoin consiste en que
habrá una cantidad finita de Bitcoins en el mundo y no se crearán más. |eed|,
no tiene un organismo estatal que controle la acuñación (metafórica, claro) de
moneda. Hay quien cree que la situación actual lo más probable es que `se
revierta`_. Para ello, alude a la `ley de Gresham y la de Thier`_ en la
economía.

.. _se revierta: https://twitter.com/sal__ohcin/status/973394227957784576
.. _ley de Gresham y la de Thier: https://en.wikipedia.org/wiki/Gresham%27s_law

En la jerga de las criptomonedas, suelen llaman **altcoins** a las
criptomonedas que no son Bitcoin. |ppej|, al analizar la capitalización
bursátil del mercado de las altcoins.

